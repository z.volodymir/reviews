document.addEventListener('DOMContentLoaded', () => {

	// Splide

	new Splide( '.splide', {
		pagination: false,
		gap: '20px',
		autoplay: true,
		interval: 5000,
		pauseOnFocus: false,
		arrowPath: 'M15.338 12.7336L3.40982 0.805791C2.65104 0.0466466 1.42082 0.0466464 0.662409 0.80579C-0.0960606 1.56426 -0.0960608 2.79442 0.662408 3.55283L11.217 14.1071L0.662712 24.661C-0.095758 25.4198 -0.0957582 26.6499 0.662711 27.4083C1.42118 28.1671 2.65134 28.1671 3.41012 27.4083L15.3383 15.4803C15.7175 15.1009 15.9069 14.6042 15.9069 14.1072C15.9069 13.61 15.7172 13.1128 15.338 12.7336Z',
		classes: {
			arrows: 'arrows',
			arrow : 'arrow',
			prev  : 'arrow--prev',
			next  : 'arrow--next',
		}
	} ).mount();

	//*****************************

	const slideItems = document.querySelectorAll('.reviews-item');
	const prev = document.querySelector('.arrow--prev');
	const next = document.querySelector('.arrow--next');
	const sideRight = document.querySelector('.aside_right');

	function hideSide() {
		const observer = new MutationObserver(() => sideRight.style.zIndex = '-1');
		observer.observe(next, {
			'attributes':true,
			'attributeFilter': ['disabled']
		});
	}
	hideSide();

	function showSide() {
		slideItems.forEach(item => {
			if (!item.hasAttribute('disabled')) sideRight.style.zIndex = '3';
		});
	}

	prev.addEventListener('click', () => showSide());
})


